package com.example.xmenapp.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.map
import com.example.xmenapp.R

class PageViewModel : ViewModel() {

    private val _index = MutableLiveData<Int>()
    val text: LiveData<String> = _index.map {
        ""
    }

    fun setIndex(index: Int) {
        _index.value = index
    }
    val SECTION_IMAGES = arrayOf(
        R.drawable.colossus,
        R.drawable.cyclops,
        R.drawable.gambit,
        R.drawable.wolverine

    )
}